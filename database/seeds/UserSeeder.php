<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $faker = Faker::create('id_ID');


        for($i = 1; $i <= 100; $i++)
        {
            App\User::create([
                'username' => $faker->userName,
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
                'alamat' => $faker->address
            ]);
        }



    }
}
