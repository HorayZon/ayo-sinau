<?php

namespace App\Http\Controllers;
use App\Tarif;

use Illuminate\Http\Request;

class TarifController extends Controller
{
    public function store(request $request)
    {
        $this->validate($request, [
            'daya' => 'required',
            'tarifperkwh' => 'required'
        ]);

        $daya = $request->input('daya');
        $tarifperkwh = $request->input('tarifperkwh');

        $tarif = new Tarif([
            'daya' => $daya,
            'tarifperkwh' => $tarifperkwh
        ]);


        if ($tarif->save()) {

            return response()->json([
                'msg' => 'Berhasil membuat tarif',
                'tarif' => $tarif
            ]);

        }else {
            return response()->json([
                'msg' => 'gagal menambahkan tarif'
            ]);
        }


    }
}
