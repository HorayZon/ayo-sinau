<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penggunaan extends Model
{
    protected $primaryKey = 'id_penggunaan';
    protected $table = 'penggunaan';

    protected $fillable = [
        'id_penggunaan' , 'bulan' , 'tahun','meter_awal','meter_akhir'
    ];


    public function users(){
         $this->hasOne(User::class);
    }
}
