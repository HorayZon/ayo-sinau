<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class TarifSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $faker = Faker::create('id_ID');

        for ($i=1; $i<= 100  ; $i++) {

            App\Tarif::create([
                'daya' => $faker->numberBetween(10000,100000),
                'tarifperkwh' =>$faker->numberBetween(3000,5000)
            ]);
        }


    }
}
