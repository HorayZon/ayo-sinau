<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penggunaan;

class PenggunaanController extends Controller
{
    public function index()
    {
        return response()->json([
            'penggunaan' => Auth::user()->penggunaan()
        ], 200);
    }

    public function show($id)
    {
        $penggunaan = Penggunaan::find($id);

        if (Auth::user()->id === $penggunaan->id) {

            return response()->json([
                'msg' => 'Informasi Penggunaan',
                'penggunaan' => $penggunaan
            ], 200);

        }
    }

    
}
