<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Tarif extends Model
{
    protected $primaryKey = 'id_tarif';
    protected $table = 'tarif';

    protected $fillable = [
        'daya','tarifperkwh'
    ];

    public function users()
    {
        $this->hasOne(User::class);
    }

}
