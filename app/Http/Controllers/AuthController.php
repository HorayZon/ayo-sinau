<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use JWTAuth;
use JWTException;
use Tymon\JWTAuth\Exceptions\JWTException as TymonJWTException;
use Tymon\JWTAuth\JWTAuth as TymonJWTAuth;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:12',
            'alamat' => 'required'
        ]);

        $nama = $request->input('name');
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $alamat = $request->input('alamat');


        $user = new User([
            'username' => $username,
            'name' => $nama,
            'email' => $email,
            'password' => bcrypt($password),
            'alamat' => $alamat
        ]);

        $credentials = [
            'username' => $username,
            'password' => $password
        ];

        if ($user->save()) {

            $token = null;

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'msg' => 'Username or Password Incorrect'
                    ], 404);
                }
            } catch (JWTException $th) {
                return response()->json([
                    'msg' => 'failed_to_create_token'
                ], 400);
            }

            $user->login = [
                'href' => 'api/v1/user/login',
                'method' => 'POST',
                'params' => 'username,password'
            ];

            $response = [
                'msg' => 'User berhasil di buat',
                'user' => $user,
                'token' => $token
            ];

            return response()->json($response, 200);
        }
    }


    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:12'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');


        if ($user = User::where('username', $username)->first()) {

            $credentials = [
                'username' => $username,
                'password' => $password
            ];

            $token = null;

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'msg' => 'Username or password is Incorrect'
                    ], 404);
                }
            } catch (JWTException $th) {
               return response()->json([
                   'msg' => 'failed_to_create_token'
               ], 400);
            }


            $response = [
                'msg' => 'User berhasil login',
                'user' => $user,
                'token' => $token
            ];

            return response()->json($response, 200);

        }else {
            $response = [
                'msg' => 'Username or Password is incorrect'
            ];

            return response()->json($response, 400);
        }

        $response = [
            'msg' => 'An Error Occurred'
        ];

        return response()->json($response, 400);
    }


    public function getTheAuthenticateUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));

    }

    public function logout(request $request)
    {
        try {
            JWTAuth::invalidate($request->header('token'));

            return response()->json([
                'success' => true,
                'msg' => 'user berhasil logout'
            ]);

        } catch (JWTException $th) {
            return response()->json([
                'success' => false,
                'msg' => 'user gagal logout'
            ], 500);
        }
    }

}
